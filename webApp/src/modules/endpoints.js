const URL_BASE = "http://localhost:3003/";
const get = url => id => URL_BASE + url.replace("{id}", id);

export const getFiles = get(`files`);
export const downloadFile = get(`file/{id}`);
export const uploadFile = get(`file`);
export const removeFile = get(`file/remove/{id}`);
