import React, { Component } from "react";
import {
  FiTrash2,
  FiDownload,
  FiFile,
  FiImage,
  FiVideo,
  FiFileText,
  FiMusic,
  FiBriefcase
} from "react-icons/fi";
import "./FileRow.css";
import { downloadFile } from "../modules/endpoints";

const iconByMimeType = mime => {
  if (mime.includes("image/")) {
    return FiImage;
  }

  if (mime.includes("video/")) {
    return FiVideo;
  }

  if (mime.includes("audio/")) {
    return FiMusic;
  }

  if (
    [
      "application/x-7z-compressed",
      "application/zip",
      "application/x-rar-compressed"
    ].includes(mime)
  ) {
    return FiBriefcase;
  }

  if (
    mime.includes("text/") ||
    ["application/pdf", "application/msword"].includes(mime)
  ) {
    return FiFileText;
  }

  return FiFile;
};

class FileRow extends Component {
  removeFile = () => {
    this.props.removeFile(this.props.id);
  };

  clickSubmitButton = () => {
    document
      .getElementById(`FileRow-cont-download-submit-${this.props.id}`)
      .click();
  };

  render() {
    const FileIcon = iconByMimeType(this.props.fileData.mimetype);
    return (
      <li className="FileRow-cont">
        <FileIcon
          size={20}
          style={{ alignSelf: "center", marginRight: "20px" }}
        />
        <span className="FileRow-name">{this.props.fileName}</span>
        <div className="FileRow-actions">
          <form
            className="FileRow-action FileRow-action-download"
            action={downloadFile(this.props.id)}
            onClick={this.clickSubmitButton}
          >
            <input
              id={`FileRow-cont-download-submit-${this.props.id}`}
              type="submit"
              value="Descargar"
              className="hidden"
            />
            <FiDownload
              size={20}
              style={{ alignSelf: "center" }}
              color={"var(--main-color)"}
            />
          </form>
          <div
            className="FileRow-action FileRow-action-remove"
            onClick={this.removeFile}
          >
            <FiTrash2
              size={20}
              style={{ alignSelf: "center" }}
              color={"var(--main-color)"}
            />
          </div>
        </div>
      </li>
    );
  }
}

export default FileRow;
