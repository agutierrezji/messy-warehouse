import React, { Component } from "react";
import "./FilesTable.css";
import FileRow from "./FileRow";

class FilesTable extends Component {
  constructor(props) {
    super(props);
    this.state = { rows: this.getRows(props.files) };
  }

  componentWillReceiveProps(newProps) {
    if (newProps !== this.props) {
      this.setState({ rows: this.getRows(newProps.files) });
    }
  }

  getRows = (files = []) => {
    return files.map(file => (
      <FileRow
        key={file.id}
        id={file.id}
        fileData={file}
        fileName={file.fileName.substr(0, 60)}
        removeFile={this.props.remove.bind(this)}
      />
    ));
  };
  render() {
    return <ul className="FilesTable-cont">{this.state.rows}</ul>;
  }
}

export default FilesTable;
