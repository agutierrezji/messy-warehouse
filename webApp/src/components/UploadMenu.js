import React, { Component } from "react";
import { uploadFile } from "../modules/endpoints";
import { FiSearch } from "react-icons/fi";
import { TextField, InputAdornment } from "@material-ui/core";
import "./UploadMenu.css";

class UploadMenu extends Component {
  onFormSubmit = async ev => {
    try {
      ev.preventDefault();
      const storedData = await this.uploadFile();
      const fileInfo = await storedData.json();
      this.props.newFile(fileInfo);
    } catch (error) {
      this.props.showError(error);
    }
  };

  async uploadFile() {
    const url = uploadFile();
    const formData = new FormData();

    formData.append("file", this.state.file);
    return await fetch(url, {
      method: "POST",
      body: formData
    });
  }

  onFileChange = ev => {
    this.setState({ file: ev.target.files[0] }, () =>
      document.getElementById("uploadSubmit").click()
    );
  };

  onAddFileButton = () => {
    document.getElementById("myFile").click();
  };

  changeFilter = ev => {
    this.props.changeFilter(ev.target.value);
  };

  render() {
    return (
      <div className="UploadMenu-cont">
        <div className="search-input">
          <TextField
            onChange={this.changeFilter}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <FiSearch color={"var(--main-color)"} />
                </InputAdornment>
              )
            }}
          />
        </div>
        <form
          className="hidden"
          onSubmit={this.onFormSubmit}
          onChange={this.onFileChange}
        >
          <input type="file" name="file" id="myFile" required />
          <input type="hidden" name="id" />
          <input id="uploadSubmit" type="submit" value="Upload"></input>
        </form>
        <div className="addFileButton" onClick={this.onAddFileButton}></div>
      </div>
    );
  }
}

export default UploadMenu;
