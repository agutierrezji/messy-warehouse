import React, { Component } from "react";
import "./ConfirmationModal.css";
import { FiX, FiCheck } from "react-icons/fi";

class ConfirmationModal extends Component {
  getCancel = () => {
    if (this.props.cancel) {
      return (
        <div
          className="RemoveConfirmation-action RemoveConfirmation-action-cancel"
          onClick={this.props.cancel}
        >
          <FiX
            size={40}
            style={{ alignSelf: "center" }}
            color={this.props.errorStyle ? "" : "var(--error-color)"}
          />
        </div>
      );
    }
  };
  getAccept = () => {
    if (this.props.accept) {
      return (
        <div className="RemoveConfirmation-action" onClick={this.props.accept}>
          <FiCheck
            size={40}
            style={{ alignSelf: "center" }}
            color={"var(--main-color)"}
          />
        </div>
      );
    }
  };

  render() {
    return (
      <div
        className={`RemoveConfirmation-Blocker ${
          this.props.show ? "" : "hidden"
        }`}
      >
        <div
          className={`RemoveConfirmation-Cont ${
            this.props.errorStyle ? "errorStyle" : ""
          }`}
        >
          <h1 className="RemoveConfirmation-title">{this.props.title}</h1>
          <p className="RemoveConfirmation-text">{this.props.text}</p>
          <div className="RemoveConfirmation-actions">
            {this.getAccept()}
            {this.getCancel()}
          </div>
        </div>
      </div>
    );
  }
}

export default ConfirmationModal;
