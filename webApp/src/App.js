import React, { Component } from "react";
import "./App.css";
import FilesTable from "./components/FilesTable";
import UploadMenu from "./components/UploadMenu";
import ConfirmationModal from "./components/ConfirmationModal";
import { getFiles, removeFile } from "./modules/endpoints";

const CONFIRM_REMOVE_TEXT = "¿Seguro que quieres eliminar?";
const GENERIC_ERROR = "Se ha producido un error";

class App extends Component {
  constructor() {
    super();
    this.state = {
      files: [],
      filter: "",
      confirmationModal: {
        title: CONFIRM_REMOVE_TEXT,
        text: "",
        show: false
      }
    };
  }

  newFile = file => {
    this.state.files.push(file);
    this.forceUpdate();
  };

  async componentDidMount() {
    return this.refreshFilesFromService();
  }

  async refreshFilesFromService() {
    try {
      const response = await fetch(getFiles());
      const files = await response.json();
      this.setState({ files });
    } catch (error) {
      this.showError(error);
    }
  }

  showRemoveModal = async id => {
    const { confirmationModal, files } = this.state;
    const { fileName } = files.find(f => f.id === id);
    this.setState({
      confirmationModal: {
        ...confirmationModal,
        title: CONFIRM_REMOVE_TEXT,
        text: `${fileName} se eliminará permanentemente`,
        show: true,
        accept: this.removeElement.bind(this, id),
        cancel: this.hideModal
      }
    });
  };

  showError = error => {
    this.setState({
      confirmationModal: {
        ...this.state.confirmationModal,
        show: true,
        accept: null,
        cancel: this.hideModal,
        errorStyle: true,
        title: GENERIC_ERROR,
        text: error.message
      }
    });
  };

  hideModal = () => {
    this.setState({
      confirmationModal: { ...this.state.confirmationModal, show: false }
    });
  };

  removeElement = async id => {
    try {
      await fetch(removeFile(id));
      this.setState({
        files: this.state.files.filter(file => file.id !== id),
        confirmationModal: { ...this.state.confirmationModal, show: false }
      });
    } catch (error) {
      this.showError(error);
    }
  };

  onFilterChange = value => {
    this.setState({ filter: value });
  };

  filteredFiles = () =>
    this.state.files.filter(file =>
      file.fileName.toUpperCase().includes(this.state.filter.toUpperCase())
    );

  render() {
    const {
      confirmationModal: { show, text, title }
    } = this.state;
    return (
      <div className="App">
        <ConfirmationModal
          title={title}
          text={text}
          show={show}
          errorStyle={this.state.confirmationModal.errorStyle}
          accept={this.state.confirmationModal.accept}
          cancel={this.state.confirmationModal.cancel}
        />
        <UploadMenu
          newFile={this.newFile}
          changeFilter={this.onFilterChange}
          showError={this.showError}
        />
        <FilesTable
          files={this.filteredFiles()}
          remove={this.showRemoveModal}
        />
      </div>
    );
  }
}

export default App;
