function getErrorResponse(text = "Unexpected error") {
  if (typeof text === "object") {
    const {trace, message} = Error(text);
    return {error: message, trace};
  }
  return JSON.stringify({ error: text });
}

function getRandomId() {
  return `${Date.now().toString(36)}${Math.random()
    .toString(36)
    .substr(2, 9)}`;
}

function sendError(res, status = 500, msg) {
  res.status(status).send(getErrorResponse(msg));
  console.error(`[${new Date().toLocaleDateString()}] ERROR: ${msg}`);
  console.trace();
}

function tryCatch(tryer, catcher) {
  try {
    return tryer();
  } catch (error) {
    return catcher(error);
  }
}

function parseBuffer(buf) {
  return tryCatch(() => JSON.parse(`${buf.toString()}`), () => null);
}

module.exports = {
  getRandomId,
  sendError,
  tryCatch,
  parseBuffer
};
