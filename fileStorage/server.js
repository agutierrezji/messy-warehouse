const express = require("express");
const app = express();
const cors = require("cors");
const multer = require("multer");

const constants = require("./constants.js");
const utils = require("./utils");
const fileController = require("./fileController");

const { sendError } = utils;

const {
  readTextFile,
  removeFile,
  getFile,
  writeFile,
  readDir
} = fileController;

const { DATA_URL } = constants;

const upload = multer({ dest: DATA_URL });

app.use(express.static("static"));

app.use(cors({ origin: "*", optionsSuccessStatus: 200 }));

/////////////////////////////
/////////// API /////////////
/////////////////////////////
app.get("/file/:id", downloadFile);
app.post("/file", upload.single("file"), uploadFile);
app.get("/file/remove/:id", remove);
app.get("/files", sendDir);

/////////////////////////////
///// LAUNCH SERVER /////////
/////////////////////////////

app.listen(3003, () => {
  console.log("Server started on port 3003");
});

/////////////////////////////
//////// ACTIONS ////////////
/////////////////////////////

function downloadFile(req, res) {
  const { id } = req.params;
  return readTextFile(`${DATA_URL}/${id}.meta`)
    .then(getFile)
    .then(({ metaData, buf }) => {
      res.writeHead(200, {
        "Content-Type": metaData.mimetype,
        "Content-disposition": "attachment;filename=" + metaData.fileName,
        "Content-Length": buf.length
      });
      res.end(Buffer.from(buf));
    })
    .catch(error => sendError(res, 500, error));
}

function uploadFile(req, res) {
  const { file } = req;
  return writeFile(file)
    .then(stringData => res.send(stringData))
    .catch(error => sendError(res, 500, error));
}

function remove(req, res) {
  const { id } = req.params;
  return removeFile(id)
    .then(() => res.send(""))
    .catch(error => sendError(res, 500, error));
}

function sendDir(req, res) {
  return readDir()
    .then(results => res.send(results))
    .catch(err => sendError(res, 500, err));
}
