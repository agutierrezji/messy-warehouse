const fs = require("fs");
const constants = require("./constants");
const utils = require("./utils");

const { getRandomId, parseBuffer } = utils;

const { DATA_URL } = constants;

const allFiles = fs.readdirSync(DATA_URL);
let metaFiles = allFiles.filter(file => file.includes(".meta"));

const wrappedFsReadFile = name =>
  new Promise((resolve, reject) =>
    fs.readFile(name, (err, buf) => (err ? reject(err) : resolve(buf)))
  );

const wrappedFsUnlink = name =>
  new Promise((resolve, reject) =>
    fs.unlink(name, err => (err ? reject(err) : resolve()))
  );

const wrappedFsWriteFile = (name, stringData) =>
  new Promise((resolve, reject) =>
    fs.writeFile(name, stringData, err => (err ? reject(err) : resolve()))
  );

const wrappedFsRename = (name, newName) =>
  new Promise((resolve, reject) =>
    fs.rename(name, newName, err => (err ? reject(err) : resolve()))
  );

const combineError = msg => err => ({ ...err, msg });

function writeFile(file) {
  if (!file) {
    return Promise.reject(combineError("Not file uploaded"));
  }

  const id = getRandomId();

  const data = {
    id,
    fileName: file.originalname,
    size: file.size,
    mimetype: file.mimetype,
    date: Date.now()
  };
  const stringData = JSON.stringify(data);

  return wrappedFsWriteFile(`${DATA_URL}/${id}.meta`, stringData)
    .then(() =>
      wrappedFsRename(
        `${DATA_URL}/${file.filename}`,
        `${DATA_URL}/${file.originalname}`
      )
    )
    .then(() => metaFiles.push(data.id + ".meta") && stringData)
    .catch(combineError("Error writing file"));
}

function readTextFile(name) {
  return wrappedFsReadFile(name)
    .then(parseBuffer)
    .catch(combineError("Error getting metadata"));
}

function getFile(metaData) {
  return wrappedFsReadFile(`${DATA_URL}/${metaData.fileName}`)
    .then(buf => ({ metaData, buf }))
    .catch(combineError("Error getting file"));
}

function readDir() {
  const promises = metaFiles.map(path =>
    wrappedFsReadFile(`${DATA_URL}/${path}`)
      .then(parseBuffer)
      .catch(combineError("Error reading metas"))
  );

  return Promise.all(promises);
}

function removeFile(id) {
  return readTextFile(`${DATA_URL}/${id}.meta`)
    .then(metaData => wrappedFsUnlink(`${DATA_URL}/${metaData.fileName}`))
    .then(() => wrappedFsUnlink(`${DATA_URL}/${id}.meta`))
    .then(() => (metaFiles = metaFiles.filter(f => f !== `${id}.meta`)))
    .catch(combineError("Error deleting file"));
}

module.exports = {
  writeFile,
  readTextFile,
  readDir,
  removeFile,
  getFile
};
