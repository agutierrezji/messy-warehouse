const os = require("os");
const config = require("./config.json");

const { storagePath } = config;

const HOME_DIR = os.homedir();
const DATA_URL = `${HOME_DIR}/${storagePath}`;

module.exports = {
  DATA_URL,
};
